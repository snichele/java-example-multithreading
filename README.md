# Programmation multithreadée avec Java - Exercices et examples

## Exercice 01 - Référence au thread courant.

Illustre : Approche basique de la notion de thread.

- Créer une classe contenant une méthode main.
- Dans cette méthode main, afficher via System.out.println() le nom du thread courant.
    - Passez par une méthode statique de la classe *Thread* pour récupérer une référence au thread courant.

## Exercice 02 - Création de threads

Illustre : Extension de la classe Thread et execution multithreadée.

- Créer une classe contenant une méthode main.
- Reprendre l'exemple du cours démarrant deux threads simultanés
- Examiner la sortir console

## Exercice 03 - Problème 'Check-then-act'  utilisation de synchronized

Illustre : Une utilisation correcte du mot clé synchronized dans un cas de check-then-act

- Créer une classe contenant une méthode main.
- Récupérer le code de la classe suivante dans votre projet

        class UnsynchronizedBankAccount {

            int accountNumber;
            double accountBalance;

            public UnsynchronizedBankAccount(int accountNumber, double accountBalance) {
                this.accountNumber = accountNumber;
                this.accountBalance = accountBalance;
            }

            // to withdraw funds from the account
            public boolean withdraw(double amount) {
                double newAccountBalance;

                if (amount > accountBalance) {
                    //there are not enough funds in the account
                    return false;
                } else {
                    // Le sleep ici est nécessaire dans un but de démonstration des accès concurrent
                    // pour introduire de la desynchronization articielle entre nos threads qui sont démarrés
                    // séquentiellement
                    try {
                        Thread.currentThread().sleep((long) (Math.random() * 100));
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    newAccountBalance = accountBalance - amount;
                    accountBalance = newAccountBalance;
                    return true;
                }

            }
        }

- Créer une classe étendant thread 
    - le constructeur prend en paramètre une instance de UnsynchronizedBankAccount
    - la méthode run() effectue un retrait d'un montant égal à la balance du compte
- Dans la méthode main, créer une instance de UnsynchronizedBankAccount, puis créez une dizaine de threads recevant le compte en paramètre de constructeur.
- Executez votre programme. Que constatez-vous ?
- Corrigez la classe UnsynchronizedBankAccount en utilisant le mot clé *synchronized*.

## Exercice 04 - Producteur / Consommateur avec une blockingQueue

Illustre : La mise en oeuvre d'une architecture Producteur / Consommateur autour d'une BlockingQueue

Cas d'utilisation fictif : Vous devez rechercher sur un filesystem tout les fichiers '.txt' pour les indexer dans
un index de moteur de recherche. Afin d'optimiser au maximum le traitement, vous optez pour une séparation du traitement
en plusieurs threads. Une partie des threads se chargera de rechercher les fichiers '.txt' sur le disque, pendant qu'une autre partie
indexera les fichiers trouvés.

Pour se faire :

- Créer deux classes, *FileCrawler* et *Indexer* implémentant Runnable
    - FileCrawler doit 
        - explorer les répertoires récursivement, à partir d'un répertorie racine, passé en paramètre du constructeur
        - a chaque fichier '.txt' trouvé, le publier dans une BlockingQueue, passée en paramètre du constructeur
    - Indexer doit
        - prendre chaque entrée dans la BlockingQueue une par une et la traiter (simuler une indexation 'lente' avec un *sleep()*)
- Créer une classe contenant une méthode main qui va
    - récupérer un pool de thread via l'*ExecutorService*
    - soumettre une ou plusieurs taches de FileCrawler
    - soumettre une ou plusieurs taches d'Indexer
- Expérimentez avec le concept, et vérifiez le comportement.

