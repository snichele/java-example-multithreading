package bitbucket.snichele.java.example.multithreading;

// An incorrect implementation of a producer and consumer. 
public class ProducerConsummer {

    public static void main(String args[]) {
//        BadSharedClass bad = new BadSharedClass();
//        new Producer(bad);
//        new Consumer(bad);

        GoodSharedClass good = new GoodSharedClass();
        new Producer(good);
        new Consumer(good);        
    }

    static class Producer implements Runnable {

        SharedClass q;

        Producer(SharedClass q) {
            this.q = q;
            new Thread(this, "Producer").start();
        }

        public void run() {
            int i = 0;
            while (i < 101) {
                q.put(i++);
            }
        }
    }

    static class Consumer implements Runnable {

        SharedClass q;

        Consumer(SharedClass q) {
            this.q = q;
            new Thread(this, "Consumer").start();
        }

        public void run() {
            while (q.get() < 100) {
                q.get();
            }
        }
    }

    static interface SharedClass {

        int get();

        void put(int n);
    }

    static class BadSharedClass implements SharedClass {

        int n;

        public synchronized int get() {
            System.out.println("Got: " + n);
            return n;
        }

        public synchronized void put(int n) {
            this.n = n;
            System.out.println("Put: " + n);
        }
    }

    static class GoodSharedClass implements SharedClass {

        int n;
        boolean valueHasMutated = false;

        public synchronized int get() {
            if (!valueHasMutated) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
            }
            System.out.println("Got: " + n);
            valueHasMutated = false;
            notify();
            return n;
        }

        public synchronized void put(int n) {
            if (valueHasMutated) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
            }
            this.n = n;
            valueHasMutated = true;
            System.out.println("Put: " + n);
            notify();
        }
    }
}
