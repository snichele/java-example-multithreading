package bitbucket.snichele.java.example.multithreading;

public class SynchronizedExample {

    public static void main(String[] args) {
        UnsynchronizedBankAccount ba = new UnsynchronizedBankAccount(5, 100);
        for (int i = 0; i < 10; i++) {
            new ClientThread("" + i, ba).start();
        }
    }
}

class UnsynchronizedBankAccount {

    int accountNumber;
    double accountBalance;

    public UnsynchronizedBankAccount(int accountNumber, double accountBalance) {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    // to withdraw funds from the account
    public synchronized boolean withdraw(double amount) {
        double newAccountBalance;

        if (amount > accountBalance) {
            //there are not enough funds in the account
            return false;
        } else {
            // Le sleep ici est nécessaire dans un but de démonstration des accès concurrent
            // pour introduire de la desynchronization articielle entre nos threads qui sont démarrés
            // séquentiellement
            try {
                Thread.currentThread().sleep((long) (Math.random() * 100));
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            newAccountBalance = accountBalance - amount;
            accountBalance = newAccountBalance;
            return true;
        }

    }
}

class ClientThread extends Thread {

    UnsynchronizedBankAccount so;

    public ClientThread(String str, UnsynchronizedBankAccount so) {
        super(str);
        this.so = so;
    }

    @Override
    public void run() {
        boolean succes = so.withdraw(90);
        System.out.println("Balance is " + so.accountBalance);
    }
}
