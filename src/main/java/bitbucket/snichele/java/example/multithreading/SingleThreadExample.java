package bitbucket.snichele.java.example.multithreading;

public class SingleThreadExample 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello from the thread " + Thread.currentThread().getName() );
    }
}
