package bitbucket.snichele.java.example.multithreading;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static bitbucket.snichele.java.example.multithreading.logging.Loggers.INDEXATION_LOGGER;

public class ProducerConsummerWithConcurrentApi {

    private final static FileFilter FILTER = new FileFilter() {
        public boolean accept(File file) {
            return file.isDirectory() || file.getName().endsWith(".txt");
        }
    };

    public static void main(String args[]) {
        INDEXATION_LOGGER.info("Main thread started");
        
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        BlockingQueue<File> queue = new LinkedBlockingQueue<File>();

        newFixedThreadPool.submit(new FileCrawler(queue, FILTER, new File("C:\\Windows\\System32")));

        newFixedThreadPool.submit(new Indexer(queue));

        newFixedThreadPool.shutdown();
    }
}

class FileCrawler implements Runnable {

    private final BlockingQueue<File> fileQueue;
    private final FileFilter fileFilter;
    private final File root;
    private final static Logger LOGGER = LoggerFactory.getLogger("MAIN");

    public FileCrawler(
            BlockingQueue<File> fileQueue, FileFilter fileFilter, File root) {
        this.fileQueue = fileQueue;
        this.fileFilter = fileFilter;
        this.root = root;
    }

    public void run() {
        try {
            LOGGER.info("Started crawling at " + root);
            crawl(root);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        LOGGER.info("Crawler thread died.");
    }

    private void crawl(File root) throws InterruptedException {
        File[] entries = root.listFiles(fileFilter);
        if (entries != null) {
            for (File entry : entries) {
                if (entry.isDirectory()) {
                    crawl(entry);
                } else {
                    fileQueue.put(entry);
                    LOGGER.info("Added for indexation " + entry);
                }
            }
        }
    }
}

class Indexer implements Runnable {

    private final BlockingQueue<File> queue;
    private final static Logger LOGGER = LoggerFactory.getLogger("MAIN");

    public Indexer(BlockingQueue<File> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            LOGGER.info("Start indexing...");
            while (true) {
                LOGGER.debug(" Queue is {} and {} ", queue, queue);
                indexFile(queue.take());
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void indexFile(File take) throws InterruptedException {
        LOGGER.debug("Indexed " + take);
        Thread.currentThread().sleep(1000);
    }
}
