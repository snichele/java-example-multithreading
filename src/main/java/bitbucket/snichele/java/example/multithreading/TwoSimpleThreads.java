package bitbucket.snichele.java.example.multithreading;

public class TwoSimpleThreads {

    public static void main(String[] args) {
        new SimpleThread("Hello").start();
        new SimpleThread("world").start();
        System.out.println("Done !");
    }
}

class SimpleThread extends Thread {

    public SimpleThread(String str) {
        super(str);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            try {
                Thread.currentThread().sleep((long) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Hello from the thread " + Thread.currentThread().getName());
    }
}
